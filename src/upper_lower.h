#ifndef _UPPER_LOWER_H_
#define _UPPER_LOWER_H_

#include <iostream>
#include "matrix.h"

int lowerTriangle(double **, int , int , int & ) ;

/*int upperTriangle(double **, int , int , int & ) ;*/
Matrix GaussJordan(const Matrix& ) ;

#endif // _UPPER_LOWER_H_
