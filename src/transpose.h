#ifndef _TRANSPOSE_H
#define _TRANSPOSE_H

#include <iostream>
#include "upper_lower.h"
#include "matrix.h"

Matrix transposeMatrix (const Matrix& ) ;

#endif // _TRANSPOSE_H
